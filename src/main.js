import { createApp } from "vue";
import App from "./App.vue";
import router from "./router";
import { library } from "@fortawesome/fontawesome-svg-core";
import {
    faArrowRight,
    faHome,
    faArrowAltCircleLeft,
    faChartPie,
    faSlidersH,
    faUserFriends,
    faChevronCircleRight,
} from "@fortawesome/free-solid-svg-icons";
import { faGoogle } from "@fortawesome/free-brands-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/vue-fontawesome";

library.add(
    faArrowRight,
    faGoogle,
    faHome,
    faChevronCircleRight,
    faArrowAltCircleLeft,
    faChartPie,
    faSlidersH,
    faUserFriends
);

createApp(App)
    .use(router)
    .component("font-awesome-icon", FontAwesomeIcon)
    .mount("#app");
