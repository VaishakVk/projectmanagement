import { createWebHistory, createRouter } from "vue-router";
import Auth from "../pages/Home/Auth.vue";
import ForgetPassword from "../pages/Home/ForgetPassword.vue";
import UpdatePassword from "../pages/Home/UpdatePassword.vue";
import Project from "../pages/Project/Project.vue";

const routes = [
    {
        path: "/signup",
        name: "Signup",
        component: Auth,
    },
    {
        path: "/login",
        name: "Login",
        component: Auth,
    },
    {
        path: "/forget-password",
        name: "ForgetPassword",
        component: ForgetPassword,
    },
    {
        path: "/set-password",
        name: "SetPassword",
        component: UpdatePassword,
    },
    {
        path: "/project",
        name: "Project",
        component: Project,
    },
];

const router = createRouter({
    history: createWebHistory(),
    routes,
});

export default router;
